#' @title Retrieve selected chemistry data from a USGS sampling location
#'
#' @description \code{retrieve_usgs_chem} returns selected USGS chemistry data
#' from USGS sampling location from a prescribed date to most recent available.
#'
#' @note The following formatting and/or filtering is applied to returned USGS
#' data:
#' * PrecisionValue is forced to type numeric
#' * data where ResultMeasureValue is missing are exluded
#' * ActivityMediaName of type `tissue` are excluded
#'
#' @param station_id
#'  (character) The quoted USGS sampling location in the form of
#'  "USGS-06611500".
#' @param parameters
#'  (character) The quoted 5-digit parameter_cd code(s) associted with the
#'  analytes of interest. Defaults to `firearea::usgs_parameter_codes_selected`
#' @param start_date
#'  (character) The quoted desired start date (default = "1980-01-01").
#'
#' @importFrom dataRetrieval readWQPqw
#' @importFrom purrr possibly
#' @importFrom dplyr mutate
#' @importFrom dplyr filter
#' @importFrom dplyr rename
#'
#' @examples
#' \dontrun{
#'
#' firearea::retrieve_usgs_chem(
#'  station_id = "USGS-06611500",
#'  parameters = c("91504", "00613")
#' )
#'
#' }
#'
#' @export
#'
retrieve_usgs_chem <- function(
  station_id,
  parameters = NULL,
  start_date = "1980-01-01"
) {

  if (is.null(parameters)) {

    parameters <- firearea::usgs_parameter_codes_selected$parameter_cd

  }

  readWQPqw_possibly <- purrr::possibly(
    .f        = dataRetrieval::readWQPqw,
    otherwise = NULL
  )

  water_chem <- readWQPqw_possibly(
    siteNumbers = station_id,
    parameterCd = parameters,
    startDate   = start_date
  )


  if (!is.null(water_chem)) {

    water_chem <- water_chem |> 
      dplyr::mutate(PrecisionValue = as.numeric(PrecisionValue)) |> 
      dplyr::rename(usgs_site = MonitoringLocationIdentifier) |>
      dplyr::filter(
        !is.na(ResultMeasureValue),
        !grepl("tissue", ActivityMediaName, ignore.case = TRUE)
      )

  }

  return(water_chem)

}
