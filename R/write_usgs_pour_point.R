#' @title Write the geometry of USGS pour point to a database
#'
#' @description Drawing on \code{dataRetrieval::findNLDI},
#' \code{write_usgs_pour_point} writes the location of a USGS sampling location
#' and corresponding metadata to a prescribed database schema and table.
#'
#' @note Geometry CRS is 4326.
#'
#' @param db_connection
#'  (character) Within the R environment, the unquoted connection to a database
#' @param usgs_site
#'  (character) The quoted USGS sampling location in the form of "06611500"
#' @param schema
#'  (character) The quoted name of the target database schema where data should
#'  be written
#' @param table
#'  (character) The quoted name of the target database table where data should
#'  be written
#'
#' @importFrom dataRetrieval findNLDI
#' @importFrom sf st_is_empty st_area
#' @importFrom DBI dbWriteTable
#'
#' @export
#'
write_usgs_pour_point <- function(
  db_connection = pg,
  usgs_site,
  schema        = "firearea",
  table         = "pour_points"
) {

  single_catchment <- dataRetrieval::findNLDI(
    nwis = usgs_site,
    nav  = "UM",
    find = "basin"
  )

  if (!is.null(single_catchment$origin)) {

    single_catchment <- firearea::nldi_to_frame(single_catchment, type = "point")
    single_catchment <- firearea::validate_sf_objects(single_catchment)

    if (!sf::st_is_empty(single_catchment$geometry)) {

        DBI::dbWriteTable(
          conn      = db_connection,
          name      = c(schema, table),
          value     = single_catchment,
          overwrite = FALSE,
          append    = TRUE,
          row.names = FALSE
        )

    } else {

      message("empty geometry: ", usgs_site)

    }

  } else {

    message("failed to find origin: ", usgs_site)

  }

}
