---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->



## firearea: tools to aid the assessment of watershed disturbance (notably wildfire) on lotic systems

The `firearea` package provides a suite of R tools to aid the assessment of watershed disturbance (notably wildfire) on lotic systems. The primary user-relevant features of `firearea` include, among others:

- *watershed delineation*: delineate the watershed above a specified point in a lotic system using one of two services provided by the USGS: `StreamStats` or `nhdplusTools`
- *format* fire perimeter data
- *crop fire perimeter data* to within a specified region (watershed), and calculate fire-area statistics within the region
- *generate a flowline buffer* at specified stream lengths above and below a sampling location
- identify USGS gages featuring discharge data within a prescribed area
- harvest USGS discharge data from a prescribed station with appropriate formatting

See the vignettes for example workflows, particularly a series of related vignettes detailing a project in which fire details, land use and land cover, water chemistry, and others are summarised for catchments corresponding to USGS gaging sites.

Users should pay close attention to the coordinate reference system (CRS) of provided and returned data. Primary interaction is with USGS resources, which employ, generally, a mix of [EPSG 4269](https://epsg.io/4269) and [EPSG 4326](https://epsg.io/4326) CRSs.


### installation

Install from GitLab:

``` r
devtools::install_gitlab("srearl/firearea")
```
