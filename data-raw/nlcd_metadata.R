## code to prepare `nlcd_metadata` dataset goes here

nlcd_metadata_xml <- xml2::read_xml(url("https://www.mrlc.gov/downloads/sciweb1/shared/mrlc/metadata/nlcd_2019_land_cover_l48_20210604.xml"))

nlcd_attributes <- xml2::xml_find_all(
  x = nlcd_metadata_xml,
  xpath = ".//edom"
)

nlcd_attributes_list <- nlcd_attributes |>
  xml2::as_list()

nlcd_metadata <- tibble::tibble(nodeset = nlcd_attributes_list) |>
  tidyr::unnest_wider(nodeset) |>
  dplyr::select(edomv, edomvd) |>
  tidyr::unnest_wider(edomv) |>
  dplyr::rename(value = 1) |>
  tidyr::unnest_wider(edomvd) |>
  dplyr::rename(description = 2) |>
  dplyr::mutate(
    type = dplyr::case_when(
      grepl("-", description) ~ stringr::str_extract(description, ".+?(?=-)"),
      TRUE ~ description
      ),
    type = stringr::str_trim(type, side = c("both")),
    type = gsub(",", ";", type),
    type = gsub("/", "; ", type)
  )

usethis::use_data(nlcd_metadata, overwrite = TRUE)
