---
title: "delineate the catchment above a prescribed location with nhdplusTools"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{delineate the catchment above a prescribed location with nhdplusTools}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(firearea)
```

### watershed delineation

`firearea` employs the `nhdplusTools` package to allow the user to delineate watershed area a prescribed stream length (km) above any arbitrary point along a lotic system. Input to `delineate_catchment` is a set of `sf` coordinates, either generated or derived from an existing spatial object. The ability to prescribe a stream length above a specified point for watershed delineation provides the user tremendous flexibility in establishing the watershed area of interest. This is particularly useful for large systems where watershed feature or disturbance far upstream of the sampling location may not be of interest. For smaller catchments, where the entire watershed is typically of interest, the stream length above a point can be set arbitrarily high such that the entire watershed is delineated. For example, the following code chunks delineate watersheds 10, 100, and 1000 km above the Dos S Ranch along Sycamore Creek, AZ, USA. Because the entire watershed is delineated when the stream length of interest above Dos S Ranch is 100 km, delineating the watershed 1000 km above Dos S Ranch yields an identical output.

Generate a `sf` object corresponding to the location of the Dos S Ranch along Sycamore Creek, AZ, USA.

```{r dos-s, eval = FALSE, echo = TRUE}

locations <- tibble::tibble(
  site      = "SS",
  longitude = "-111.5091",
  latitude  = "33.74872"
) |>
sf::st_as_sf(
  coords = c("longitude", "latitude"),
  crs    = 4326
) |>
sf::st_transform(crs = 4269)

```

Delineate the watershed 10 km above the Dos S Ranch:

```{r dos-s-10, eval = FALSE, echo = TRUE}

ss_10 <- firearea::delineate_catchment(
  location_sf         = locations,
  stream_length       = 10,
  location_identifier = "site"
)
```

Delineate the watershed 100 km above the Dos S Ranch:

```{r dos-s-100, eval = FALSE, echo = TRUE}

ss_100 <- firearea::delineate_catchment(
  location_sf         = locations,
  stream_length       = 100,
  location_identifier = "site"
)
```

Delineate the watershed 1000 km above the Dos S Ranch:

```{r dos-s-1000, eval = FALSE, echo = TRUE}

ss_1000 <- firearea::delineate_catchment(
  location_sf         = locations,
  stream_length       = 1000,
  location_identifier = "site"
)
```

In this example, the watershed delineated 1000 km above the Dos S Ranch is not plotted because the entire watershed above that point has already been delineated at a distance 100 km above thus `ss_100` and `ss_1000` result in identical polygon.

```{r watershed-delineation, fig.align = 'center', out.width = "75%", fig.cap = "Sycamore Creek watershed delinated at two different stream lengths (10 and 100 km) above Dos S ranch", eval = TRUE, echo = FALSE}
knitr::include_graphics("man/figures/sycamore_creek_10_and_100.jpg")
```

### format fire perimeter data

`format_fire_perimeters` formats fire perimeter data downloaded from either the [NIFC](https://data-nifc.opendata.arcgis.com/datasets/4454e5d8e8c44b0280258b51bcf24794_0/explore?location=31.568028%2C-110.534920%2C10.00) or [MTBS](https://edcintl.cr.usgs.gov/downloads/sciweb1/shared/MTBS_Fire/data/composite_data/fod_pt_shapefile/mtbs_fod_pts_data.zip) with the appropriate coordinate reference system, and allows the user to filter fire occurrences within a data ranger. `format_fire_perimeters` accepts fire perimeter data downloaded from the NIFC or MTBS as an `sf` object. The `sf` object is assigned the appropriate CRS to ease interfacing with USGS data (i.e., EPSG 4269). The user can provide lower and upper date ranges (as year (integer)) to filter the data to a desired date range. The user may identify the state of interest to crop the fires to the boundary of that state - this is highly recommended as downstream processing with the fire perimeter data is computationally expensive.


```{r mtbs-fire-perimiters, eval = FALSE, echo = TRUE}

mtbs_fire_perimiters <- sf::st_read(
  dsn   = "~/seagate/Desktop/gis/MTBS/",
  layer = "mtbs_perims_DD"
  )

mtbs_fire_perimiters <- firearea::format_fire_perimeters(
  fire_layer = mtbs_fire_perimiters,
  state_code = "az"
)
```

### crop fires to study watershed

`crop_fires_to_watershed` accepts a `sf` layer of fire perimeter boundaries with appropriate formatting as outlined in the `format_fire_perimeters` function in the `firearea` package, and a watershed boundary as an sf layer. The function will iterate through each fire (row) of the provided fire perimeter data layer to crop the area of the fire to the boundary of the watershed. Additionally, the area and per cent of the fire within the watershed are calculated. This computationally intensive approach is required to maintain the identity and other details of individual fires that can be lost when performing an intersection of multiple polygons within another polygon in a single call.

```{r fires-in-watershed, eval = FALSE, echo = TRUE}

catchment_fires <- firearea::crop_fires_to_watershed(
  fires_sf     = mtbs_fire_perimiters,
  watershed_sf = my_catchment
)
```
