---
title: "delineate-catchment-with-streamstats"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{delineate-catchment-with-streamstats}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---





## overview

The `delineate_catchment_streamstats` accepts location data as a pair of latitude and longitude coordinates. Additionally, the user is able to identify a helpful identifier to the location by providing text or reference to a column name in the data that features the desired identifier. In keeping with the `firearea` package and connection to USGS resources, the resulting spatial layer has a CRS of EPSG [4269](https://www.spatialreference.org/ref/epsg/4269/).

## single-catchment delineation

### Dos S Ranch at Sycamore Creek, Arizona

In this example, we will delineate the catchment of Sycamore Creek, Arizona above the Dos S ranch, a renowned study site in the field of lotic ecology. We will pass the known coordinates as latitude and longitude directly as parameters, and provide an identifier `syc_ss` for the output.


```r

syc_ss <- firearea::delineate_catchment_streamstats(
  longitude           = "-111.5091",
  latitude            = "33.74872",
  state_code          = "az",
  location_identifier = "syc_ss"
)
#> Error in firearea::delineate_catchment_streamstats(longitude = "-111.5091", : a valid longitude is required
```
<br>
<br>




<div class="figure" style="text-align: center">
<img src="syc_ss_mapview.png" alt="Plot of `syc_ss`: Sycamore Creek, AZ catchment delineated above the Dos S Ranch"  />
<p class="caption">Plot of `syc_ss`: Sycamore Creek, AZ catchment delineated above the Dos S Ranch</p>
</div>

### catchment characteristics

## this functionality is under development!

Because `delineate_catchment_streamstats` stores catchment details, we can use `streamstats::computeFlowStats` to assess certain catchment characteristics. Note that streamstats::computeFlowStats requires the `dplyr` library.


```r
library(dplyr)

syc_ss_stats <- streamstats::computeFlowStats(
  workspaceID = syc_ss$workspaceID,
  rcode       = "az",
  simplify    = TRUE
)

```

The output from `computeFlowStats` can be verbose so let us just look at the first few lines of output:



In this example, `computeFlowStats` returns statistics for 48 variables among two categories (Peak-flow and Flood-volumn statistics), samples of which are displayed below.



## delineating multiple catchments

Perhaps the greatest utility of `delineate_catchment_streamstats` is the ability to delineate catchments from multiple locations in a scripted workflow.

### New Mexico headewater streams

For this example, we will work with a suite of coordinates corresponding to sampling locations on small streams in northern New Mexico.

<!--html_preserve--><div id="mlxnzvnnmm" style="padding-left:0px;padding-right:0px;padding-top:10px;padding-bottom:10px;overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>#mlxnzvnnmm table {
  font-family: system-ui, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji';
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

#mlxnzvnnmm thead, #mlxnzvnnmm tbody, #mlxnzvnnmm tfoot, #mlxnzvnnmm tr, #mlxnzvnnmm td, #mlxnzvnnmm th {
  border-style: none;
}

#mlxnzvnnmm p {
  margin: 0;
  padding: 0;
}

#mlxnzvnnmm .gt_table {
  display: table;
  border-collapse: collapse;
  line-height: normal;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#mlxnzvnnmm .gt_caption {
  padding-top: 4px;
  padding-bottom: 4px;
}

#mlxnzvnnmm .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#mlxnzvnnmm .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 3px;
  padding-bottom: 5px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#mlxnzvnnmm .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#mlxnzvnnmm .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mlxnzvnnmm .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#mlxnzvnnmm .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#mlxnzvnnmm .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#mlxnzvnnmm .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#mlxnzvnnmm .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#mlxnzvnnmm .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#mlxnzvnnmm .gt_spanner_row {
  border-bottom-style: hidden;
}

#mlxnzvnnmm .gt_group_heading {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  text-align: left;
}

#mlxnzvnnmm .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#mlxnzvnnmm .gt_from_md > :first-child {
  margin-top: 0;
}

#mlxnzvnnmm .gt_from_md > :last-child {
  margin-bottom: 0;
}

#mlxnzvnnmm .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#mlxnzvnnmm .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
}

#mlxnzvnnmm .gt_stub_row_group {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: top;
}

#mlxnzvnnmm .gt_row_group_first td {
  border-top-width: 2px;
}

#mlxnzvnnmm .gt_row_group_first th {
  border-top-width: 2px;
}

#mlxnzvnnmm .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#mlxnzvnnmm .gt_first_summary_row {
  border-top-style: solid;
  border-top-color: #D3D3D3;
}

#mlxnzvnnmm .gt_first_summary_row.thick {
  border-top-width: 2px;
}

#mlxnzvnnmm .gt_last_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mlxnzvnnmm .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#mlxnzvnnmm .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#mlxnzvnnmm .gt_last_grand_summary_row_top {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-bottom-style: double;
  border-bottom-width: 6px;
  border-bottom-color: #D3D3D3;
}

#mlxnzvnnmm .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#mlxnzvnnmm .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#mlxnzvnnmm .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#mlxnzvnnmm .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#mlxnzvnnmm .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#mlxnzvnnmm .gt_sourcenote {
  font-size: 90%;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 5px;
  padding-right: 5px;
}

#mlxnzvnnmm .gt_left {
  text-align: left;
}

#mlxnzvnnmm .gt_center {
  text-align: center;
}

#mlxnzvnnmm .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#mlxnzvnnmm .gt_font_normal {
  font-weight: normal;
}

#mlxnzvnnmm .gt_font_bold {
  font-weight: bold;
}

#mlxnzvnnmm .gt_font_italic {
  font-style: italic;
}

#mlxnzvnnmm .gt_super {
  font-size: 65%;
}

#mlxnzvnnmm .gt_footnote_marks {
  font-size: 75%;
  vertical-align: 0.4em;
  position: initial;
}

#mlxnzvnnmm .gt_asterisk {
  font-size: 100%;
  vertical-align: 0;
}

#mlxnzvnnmm .gt_indent_1 {
  text-indent: 5px;
}

#mlxnzvnnmm .gt_indent_2 {
  text-indent: 10px;
}

#mlxnzvnnmm .gt_indent_3 {
  text-indent: 15px;
}

#mlxnzvnnmm .gt_indent_4 {
  text-indent: 20px;
}

#mlxnzvnnmm .gt_indent_5 {
  text-indent: 25px;
}
</style>
<table class="gt_table" data-quarto-disable-processing="false" data-quarto-bootstrap="false">
  <thead>
    <tr class="gt_col_headings">
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1" style="font-size: medium;" scope="col" id="latitude">latitude</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1" style="font-size: medium;" scope="col" id="longitude">longitude</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1" style="font-size: medium;" scope="col" id="Stream">Stream</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.86583</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.5978</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">Redondo Creek</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.84845</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.4907</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">East Fork Jemez River</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.97345</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.5969</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">San Antonio - West</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.96263</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.4906</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">San Antonio Creek - Toledo</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.96387</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.4899</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">Indios Creek</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.96387</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.4899</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">Indios Creek - Post Fire (Below Burn)</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.99384</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.4740</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">Indios Creek - above burn</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.91417</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.6064</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">Sulfur Creek</td></tr>
    <tr><td headers="latitude" class="gt_row gt_right" style="font-size: small;">35.97269</td>
<td headers="longitude" class="gt_row gt_right" style="font-size: small;">-106.5976</td>
<td headers="Stream" class="gt_row gt_left" style="font-size: small;">san_antonio_custom</td></tr>
  </tbody>
  
  
</table>
</div><!--/html_preserve-->

To prevent our run from being derailed by an error, which are more frequent than desired, best is to put `delineate_catchment_streamstats` in a wrapper to handle fails. We can handle failed delineations in a follow-up step.


```r
dcss_possibly <- purrr::possibly(
  .f = delineate_catchment_streamstats,
  otherwise = NULL
)
```

In the code below, we will split the tibble of New Mexico sampling locations into a list, then loop over the elements in that list with our wrapped `delineate_catchment_streamstats` function (i.e., `dcss_possibly`), passing the sampling point latitude, longitude, state, and location identifier as appropriate.


```r
new_mexico_ws <- split(
  x = new_mexico_points,
  f = new_mexico_points$Stream
) |>
{\(df) purrr::map_df(.x = df, ~ dcss_possibly(longitude = .x$longitude, latitude = .x$latitude, state_code = "nm", location_identifier = .x$Stream))}()
```



<br>
<br>

<div class="figure" style="text-align: center">
<img src="new_mexico_catchment.png" alt="Plot of `new_mexico_ws`: New Mexico catchments delineated above multiple points"  />
<p class="caption">Plot of `new_mexico_ws`: New Mexico catchments delineated above multiple points</p>
</div>
