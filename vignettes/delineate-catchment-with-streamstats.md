---
title: "delineate-catchment-with-streamstats"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{delineate-catchment-with-streamstats}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---




```r
library(firearea)
```

## overview

The `delineate_catchment_streamstats` accepts location data as a pair of latitude and longitude coordinates. Additionally, the user is able to identify a helpful identifier to the location by providing text or reference to a column name in the data that features the desired identifier.

In keeping with the `firearea` package and connection to USGS resources, the resulting spatial layer has a CRS of EPSG [4269](https://www.spatialreference.org/ref/epsg/4269/).

## example: Dos S Ranch at Sycamore Creek, Arizona

In this example, we will delineate the catchment of Sycamore Creek, Arizona above the Dos S ranch, a renowned study site in the field of lotic ecology. We will pass the known coordinates as latitude and longitude directly as parameters, and provide an identifier `syc_ss` for the output.


```r

syc_ss <- firearea::delineate_catchment_streamstats(
  longitude = -111.5091,
  latitude = 33.74872,
  state_code = "az",
  location_identifier = "syc_ss"
)
#> https://streamstats.usgs.gov/streamstatsservices/watershed.geojson?rcode=AZ&xlocation=-111.5091&ylocation=33.74872&includeparameters=false&includeflowtypes=true&includefeatures=true&crs=4326&simplify=FALSEReading layer `streamstats_watershed' from data source `/tmp/streamstats_watershed.geojson' using driver `GeoJSON'
#> Simple feature collection with 1 feature and 119 fields
#> Geometry type: POLYGON
#> Dimension:     XY
#> Bounding box:  xmin: -111.5647 ymin: 33.7436 xmax: -111.3716 ymax: 34.0064
#> Geodetic CRS:  WGS 84
#> 
#> returning watershed polygon for coordinate: -111.5091 33.74872
```
Map of Sycamore Creek catchment delineated above the Dos S Ranch.


```r
knitr::include_graphics("syc_ss_mapview.png")
```

![plot of chunk syc-ss-map](syc_ss_mapview.png)

### catchment characteristics

Because `delineate_catchment_streamstats` stores catchment details, we can use `streamstats::computeFlowStats` to assess certain catchment characteristics. Note that streamstats::computeFlowStats requires the `dplyr` library.


```r

library(dplyr)

syc_ss_stats <- streamstats::computeFlowStats(
  workspaceID = syc_ss$workspaceID,
  rcode = "az",
  simplify = TRUE
)
#> https://streamstats.usgs.gov/streamstatsservices/flowstatistics.json?rcode=az&workspaceID=AZ20211115181121442000
```

The output from computeFlowStats can be verbose so let us just look at the first few lines of output:

<!--html_preserve--><div id="uuzzywkafy" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#uuzzywkafy .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#uuzzywkafy .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#uuzzywkafy .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#uuzzywkafy .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#uuzzywkafy .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#uuzzywkafy .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#uuzzywkafy .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#uuzzywkafy .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#uuzzywkafy .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#uuzzywkafy .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#uuzzywkafy .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#uuzzywkafy .gt_group_heading {
  padding: 8px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#uuzzywkafy .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#uuzzywkafy .gt_from_md > :first-child {
  margin-top: 0;
}

#uuzzywkafy .gt_from_md > :last-child {
  margin-bottom: 0;
}

#uuzzywkafy .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#uuzzywkafy .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 12px;
}

#uuzzywkafy .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#uuzzywkafy .gt_first_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
}

#uuzzywkafy .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#uuzzywkafy .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#uuzzywkafy .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#uuzzywkafy .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#uuzzywkafy .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#uuzzywkafy .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding: 4px;
}

#uuzzywkafy .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#uuzzywkafy .gt_sourcenote {
  font-size: 90%;
  padding: 4px;
}

#uuzzywkafy .gt_left {
  text-align: left;
}

#uuzzywkafy .gt_center {
  text-align: center;
}

#uuzzywkafy .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#uuzzywkafy .gt_font_normal {
  font-weight: normal;
}

#uuzzywkafy .gt_font_bold {
  font-weight: bold;
}

#uuzzywkafy .gt_font_italic {
  font-style: italic;
}

#uuzzywkafy .gt_super {
  font-size: 65%;
}

#uuzzywkafy .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 65%;
}
</style>
<table class="gt_table">
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">var_name</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">var_code</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">var_desc</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">value</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">group_name</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">group_id</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_left" style="font-size: small;">0.2-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK0_2AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 0.2% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">70800</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
    <tr><td class="gt_row gt_left" style="font-size: small;">0.5-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK0_5AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 0.5% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">50800</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
    <tr><td class="gt_row gt_left" style="font-size: small;">1-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK1AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 1% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">38600</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
    <tr><td class="gt_row gt_left" style="font-size: small;">10-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK10AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 10% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">10400</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
    <tr><td class="gt_row gt_left" style="font-size: small;">2-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK2AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 2% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">28200</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
    <tr><td class="gt_row gt_left" style="font-size: small;">20-percent AEP flood</td>
<td class="gt_row gt_left" style="font-size: small;">PK20AEP</td>
<td class="gt_row gt_left" style="font-size: small;">Maximum instantaneous flow that occurs with a 20% annual exceedance probability</td>
<td class="gt_row gt_right" style="font-size: small;">5730</td>
<td class="gt_row gt_left" style="font-size: small;">Peak-Flow Statistics</td>
<td class="gt_row gt_right" style="font-size: small;">2</td></tr>
  </tbody>
  
  
</table>
</div><!--/html_preserve-->

In this case the columns inlude var_name, var_code, var_desc, value, group_name, group_id but those may vary depending on the returned data.

## delineating multiple catchments

Perhaps the greatest utility of `delineate_catchment_streamstats` is the ability to delineate catchments from multiple locations in a scripted workflow.

### example: delineating multiple catchments in New Mexico

For this example, we will work with a suite of coordinates corresponding to sampling locations on small streams in northern New Mexico.

<!--html_preserve--><div id="sfvqrrmbrx" style="overflow-x:auto;overflow-y:auto;width:auto;height:auto;">
<style>html {
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Helvetica Neue', 'Fira Sans', 'Droid Sans', Arial, sans-serif;
}

#sfvqrrmbrx .gt_table {
  display: table;
  border-collapse: collapse;
  margin-left: auto;
  margin-right: auto;
  color: #333333;
  font-size: 16px;
  font-weight: normal;
  font-style: normal;
  background-color: #FFFFFF;
  width: auto;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #A8A8A8;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #A8A8A8;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
}

#sfvqrrmbrx .gt_heading {
  background-color: #FFFFFF;
  text-align: center;
  border-bottom-color: #FFFFFF;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#sfvqrrmbrx .gt_title {
  color: #333333;
  font-size: 125%;
  font-weight: initial;
  padding-top: 4px;
  padding-bottom: 4px;
  border-bottom-color: #FFFFFF;
  border-bottom-width: 0;
}

#sfvqrrmbrx .gt_subtitle {
  color: #333333;
  font-size: 85%;
  font-weight: initial;
  padding-top: 0;
  padding-bottom: 6px;
  border-top-color: #FFFFFF;
  border-top-width: 0;
}

#sfvqrrmbrx .gt_bottom_border {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#sfvqrrmbrx .gt_col_headings {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
}

#sfvqrrmbrx .gt_col_heading {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 6px;
  padding-left: 5px;
  padding-right: 5px;
  overflow-x: hidden;
}

#sfvqrrmbrx .gt_column_spanner_outer {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: normal;
  text-transform: inherit;
  padding-top: 0;
  padding-bottom: 0;
  padding-left: 4px;
  padding-right: 4px;
}

#sfvqrrmbrx .gt_column_spanner_outer:first-child {
  padding-left: 0;
}

#sfvqrrmbrx .gt_column_spanner_outer:last-child {
  padding-right: 0;
}

#sfvqrrmbrx .gt_column_spanner {
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: bottom;
  padding-top: 5px;
  padding-bottom: 5px;
  overflow-x: hidden;
  display: inline-block;
  width: 100%;
}

#sfvqrrmbrx .gt_group_heading {
  padding: 8px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
}

#sfvqrrmbrx .gt_empty_group_heading {
  padding: 0.5px;
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  vertical-align: middle;
}

#sfvqrrmbrx .gt_from_md > :first-child {
  margin-top: 0;
}

#sfvqrrmbrx .gt_from_md > :last-child {
  margin-bottom: 0;
}

#sfvqrrmbrx .gt_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  margin: 10px;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 1px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 1px;
  border-right-color: #D3D3D3;
  vertical-align: middle;
  overflow-x: hidden;
}

#sfvqrrmbrx .gt_stub {
  color: #333333;
  background-color: #FFFFFF;
  font-size: 100%;
  font-weight: initial;
  text-transform: inherit;
  border-right-style: solid;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
  padding-left: 12px;
}

#sfvqrrmbrx .gt_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#sfvqrrmbrx .gt_first_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
}

#sfvqrrmbrx .gt_grand_summary_row {
  color: #333333;
  background-color: #FFFFFF;
  text-transform: inherit;
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
}

#sfvqrrmbrx .gt_first_grand_summary_row {
  padding-top: 8px;
  padding-bottom: 8px;
  padding-left: 5px;
  padding-right: 5px;
  border-top-style: double;
  border-top-width: 6px;
  border-top-color: #D3D3D3;
}

#sfvqrrmbrx .gt_striped {
  background-color: rgba(128, 128, 128, 0.05);
}

#sfvqrrmbrx .gt_table_body {
  border-top-style: solid;
  border-top-width: 2px;
  border-top-color: #D3D3D3;
  border-bottom-style: solid;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
}

#sfvqrrmbrx .gt_footnotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#sfvqrrmbrx .gt_footnote {
  margin: 0px;
  font-size: 90%;
  padding: 4px;
}

#sfvqrrmbrx .gt_sourcenotes {
  color: #333333;
  background-color: #FFFFFF;
  border-bottom-style: none;
  border-bottom-width: 2px;
  border-bottom-color: #D3D3D3;
  border-left-style: none;
  border-left-width: 2px;
  border-left-color: #D3D3D3;
  border-right-style: none;
  border-right-width: 2px;
  border-right-color: #D3D3D3;
}

#sfvqrrmbrx .gt_sourcenote {
  font-size: 90%;
  padding: 4px;
}

#sfvqrrmbrx .gt_left {
  text-align: left;
}

#sfvqrrmbrx .gt_center {
  text-align: center;
}

#sfvqrrmbrx .gt_right {
  text-align: right;
  font-variant-numeric: tabular-nums;
}

#sfvqrrmbrx .gt_font_normal {
  font-weight: normal;
}

#sfvqrrmbrx .gt_font_bold {
  font-weight: bold;
}

#sfvqrrmbrx .gt_font_italic {
  font-style: italic;
}

#sfvqrrmbrx .gt_super {
  font-size: 65%;
}

#sfvqrrmbrx .gt_footnote_marks {
  font-style: italic;
  font-weight: normal;
  font-size: 65%;
}
</style>
<table class="gt_table">
  
  <thead class="gt_col_headings">
    <tr>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">latitude</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_right" rowspan="1" colspan="1">longitude</th>
      <th class="gt_col_heading gt_columns_bottom_border gt_left" rowspan="1" colspan="1">Stream</th>
    </tr>
  </thead>
  <tbody class="gt_table_body">
    <tr><td class="gt_row gt_right">35.86583</td>
<td class="gt_row gt_right">-106.5978</td>
<td class="gt_row gt_left">Redondo Creek</td></tr>
    <tr><td class="gt_row gt_right">35.84845</td>
<td class="gt_row gt_right">-106.4907</td>
<td class="gt_row gt_left">East Fork Jemez River</td></tr>
    <tr><td class="gt_row gt_right">35.97345</td>
<td class="gt_row gt_right">-106.5969</td>
<td class="gt_row gt_left">San Antonio - West</td></tr>
    <tr><td class="gt_row gt_right">35.96263</td>
<td class="gt_row gt_right">-106.4906</td>
<td class="gt_row gt_left">San Antonio Creek - Toledo</td></tr>
    <tr><td class="gt_row gt_right">35.96387</td>
<td class="gt_row gt_right">-106.4899</td>
<td class="gt_row gt_left">Indios Creek</td></tr>
    <tr><td class="gt_row gt_right">35.96387</td>
<td class="gt_row gt_right">-106.4899</td>
<td class="gt_row gt_left">Indios Creek - Post Fire (Below Burn)</td></tr>
    <tr><td class="gt_row gt_right">35.99384</td>
<td class="gt_row gt_right">-106.4740</td>
<td class="gt_row gt_left">Indios Creek - above burn</td></tr>
    <tr><td class="gt_row gt_right">35.91417</td>
<td class="gt_row gt_right">-106.6064</td>
<td class="gt_row gt_left">Sulfur Creek</td></tr>
    <tr><td class="gt_row gt_right">35.97269</td>
<td class="gt_row gt_right">-106.5976</td>
<td class="gt_row gt_left">san_antonio_custom</td></tr>
  </tbody>
  
  
</table>
</div><!--/html_preserve-->

To prevent our run from being derailed by an error, which are more frequent than desired, best is to put `delineate_catchment_streamstats` in a wrapper to handle fails. We can handle failed delineations in a follow-up step.


```r

dcss_possibly <- purrr::possibly(
  .f = delineate_catchment_streamstats,
  otherwise = NULL
)
```

In the code below, we will split the tibble of New Mexico sampling locations into a list, then loop over the elements in that list with our wrapped `delineate_catchment_streamstats` function (i.e., `dcss_possibly`), passing the sampling point latitude, longitude, state, and location identifier as appropriate.


```r

new_mexico_ws <- split(
  x = new_mexico_points,
  f = new_mexico_points$Stream
) |>
{\(df) purrr::map_df(.x = df, ~ dcss_possibly(longitude = .x$longitude, latitude = .x$latitude, state_code = "nm", location_identifier = .x$Stream))}()

```
